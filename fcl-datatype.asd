(defsystem "fcl-datatype"
  :version "0.1.0"
  :author "ekatsym"
  :license "LLGPL"
  :depends-on ("alexandria")
  :components ((:module "src"
                :serial t
                :components
                ((:file "package")
                 (:file "util")
                 (:file "defdata"))))
  :description ""
  :in-order-to ((test-op (test-op "fcl-datatype/tests"))))

(defsystem "fcl-datatype/tests"
  :author "ekatsym"
  :license ""
  :depends-on ("fcl-datatype"
               "fiveam")
  :components ((:module "tests"
                :serial t
                :components
                ((:file "fcl-datatype-test"))))
  :description "Test system for fcl-datatype"
  :perform (test-op (o s)
                    (uiop:symbol-call :fiveam '#:run!
                                      (uiop:find-symbol* '#:fcl-datatype
                                                         :fcl-datatype/tests))))
