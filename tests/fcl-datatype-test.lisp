(defpackage :fcl-datatype/tests
  (:nicknames :fcl-dt/t)
  (:use :cl :fiveam :fcl-datatype))
(in-package :fcl-dt/t)


;;; Define Data
(defdata maybe
  (nothing)
  (just t))

(defdata liszt
  (knil)
  (kons t liszt))


(def-suite fcl-datatype)
(in-suite fcl-datatype)


;;; Maybe
(test maybe-hierarchy
  (is (subtypep 'just 'maybe))
  (is (subtypep 'nothing 'maybe))
  (is (not (subtypep 'maybe 'just)))
  (is (not (subtypep 'maybe 'nothing)))
  (is (not (subtypep 'just 'nothing)))
  (is (not (subtypep 'nothing 'just))))

(test nothing-identity
  (is (data= (nothing) (nothing))))

(test just-identity
  (for-all ((n (gen-integer))
            (f (gen-float))
            (c (gen-character))
            (l (gen-list))
            (s (gen-string)))
    (is (data= (just n) (just n)))
    (is (data= (just f) (just f)))
    (is (data= (just c) (just c)))
    (is (data= (just l) (just l)))
    (is (data= (just s) (just s)))))

(test just-commutativity
  (for-all ((n (gen-integer))
            (f (gen-float))
            (c (gen-character))
            (l (gen-list))
            (s (gen-string)))
    (is (= n (just%0 (just n))))
    (is (= f (just%0 (just f))))
    (is (char= c (just%0 (just c))))
    (is (equal l (just%0 (just l))))
    (is (string= s (just%0 (just s))))))


;;; Liszt
(test liszt-hierarchy
  (is (subtypep 'kons 'liszt))
  (is (subtypep 'knil 'liszt))
  (is (not (subtypep 'liszt 'kons)))
  (is (not (subtypep 'liszt 'knil)))
  (is (not (subtypep 'knil 'kons)))
  (is (not (subtypep 'kons 'knil))))

(test liszt-identity
  ;; length 0
  (is (data= (knil) (knil)))

  ;; length 1
  (for-all ((n (gen-integer))
            (f (gen-float))
            (c (gen-character))
            (l (gen-list))
            (s (gen-string)))
    (is (data= (kons n (knil)) (kons n (knil))))
    (is (data= (kons f (knil)) (kons f (knil))))
    (is (data= (kons c (knil)) (kons c (knil))))
    (is (data= (kons l (knil)) (kons l (knil))))
    (is (data= (kons s (knil)) (kons s (knil)))))

  ;; length 2
  (for-all ((n0 (gen-integer))
            (n1 (gen-integer))
            (f0 (gen-float))
            (f1 (gen-float))
            (c0 (gen-character))
            (c1 (gen-character))
            (l0 (gen-list))
            (l1 (gen-list))
            (s0 (gen-string))
            (s1 (gen-string)))
    (is (data= (kons n0 (kons n1 (knil))) (kons n0 (kons n1 (knil)))))
    (is (data= (kons f0 (kons f1 (knil))) (kons f0 (kons f1 (knil)))))
    (is (data= (kons c0 (kons c1 (knil))) (kons c0 (kons c1 (knil)))))
    (is (data= (kons l0 (kons l1 (knil))) (kons l0 (kons l1 (knil)))))
    (is (data= (kons s0 (kons s1 (knil))) (kons s0 (kons s1 (knil))))))

  ;; length 3
  (for-all ((n0 (gen-integer))
            (n1 (gen-integer))
            (n2 (gen-integer))
            (f0 (gen-float))
            (f1 (gen-float))
            (f2 (gen-float))
            (c0 (gen-character))
            (c1 (gen-character))
            (c2 (gen-character))
            (l0 (gen-list))
            (l1 (gen-list))
            (l2 (gen-list))
            (s0 (gen-string))
            (s1 (gen-string))
            (s2 (gen-string)))
    (is (data= (kons n0 (kons n1 (kons n2 (knil)))) (kons n0 (kons n1 (kons n2 (knil))))))
    (is (data= (kons f0 (kons f1 (kons f2 (knil)))) (kons f0 (kons f1 (kons f2 (knil))))))
    (is (data= (kons c0 (kons c1 (kons c2 (knil)))) (kons c0 (kons c1 (kons c2 (knil))))))
    (is (data= (kons l0 (kons l1 (kons l2 (knil)))) (kons l0 (kons l1 (kons l2 (knil))))))
    (is (data= (kons s0 (kons s1 (kons s2 (knil)))) (kons s0 (kons s1 (kons s2 (knil))))))))

(test kons-commutativity
  ;; kar
  (for-all ((n (gen-integer))
            (f (gen-float))
            (c (gen-character))
            (l (gen-list))
            (s (gen-string)))
    (is (= n (kons%0 (kons n (knil)))))
    (is (= f (kons%0 (kons f (knil)))))
    (is (char= c (kons%0 (kons c (knil)))))
    (is (equal l (kons%0 (kons l (knil)))))
    (is (string= s (kons%0 (kons s (knil))))))

  ;; kdr
  (for-all ((n (gen-integer))
            (f (gen-float))
            (c (gen-character))
            (l (gen-list))
            (s (gen-string)))
    (is (data= (knil) (kons%1 (kons n (knil)))))
    (is (data= (knil) (kons%1 (kons f (knil)))))
    (is (data= (knil) (kons%1 (kons c (knil)))))
    (is (data= (knil) (kons%1 (kons l (knil)))))
    (is (data= (knil) (kons%1 (kons s (knil)))))))
