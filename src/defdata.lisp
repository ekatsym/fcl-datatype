(defpackage fcl-datatype.defdata
  (:nicknames :fcl-dt.defdata)
  (:use :cl)
  (:import-from
    :fcl-dt
    #:defdata
    #:data=)
  (:import-from
    :alexandria
    #:define-constant)
  (:import-from
    :fcl-dt.util
    #:make-parameters))
(in-package :fcl-dt.defdata)


;;; Definition Macro
(defstruct (algebraic-data-type (:constructor nil)
                                (:copier nil)
                                (:predicate nil)))

(defmacro defdata (name &body constructors)
  (check-type name symbol)
  (every (lambda (constructor)
           (check-type constructor list))
         constructors)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (defstruct (,name (:constructor nil)
                       (:copier nil)
                       (:include algebraic-data-type)
                       (:predicate nil)))
     ,@(mapcar (lambda (constructor)
                 (parse-constructor constructor name))
               constructors)
     ,@(mapcar #'parse-printer constructors)
     ',name))

(defun data= (data1 data2)
  (check-type data1 algebraic-data-type)
  (check-type data2 algebraic-data-type)
  (equalp data1 data2))


;;; Constructor Parser
(defun parse-constructor (constructor data-name)
  (let ((constructor-name (first constructor))
        (types (rest constructor))
        (parameters (make-parameters (length (rest constructor)))))
    `(defstruct (,constructor-name (:conc-name ,constructor-name)
                                   (:constructor ,constructor-name ,parameters)
                                   (:copier nil)
                                   (:include ,data-name)
                                   (:predicate nil))
       ,@(mapcar (lambda (type parameter)
                   `(,parameter ,parameter :type ,type :read-only t))
                 types
                 parameters))))


;;; Data Printer
(defun parse-printer (constructor)
  (let ((name (first constructor))
        (parameters (make-parameters (length (rest constructor)))))
    `(defmethod print-object ((object ,name) stream)
       (format stream
               "(~S~{ ~S~})"
               ',name
               (with-slots ,parameters object
                 (list ,@parameters))))))
