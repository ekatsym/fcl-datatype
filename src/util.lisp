(defpackage fcl-datatype.util
  (:nicknames :fcl-dt.util)
  (:use cl)
  (:import-from
    :alexandria
    #:symbolicate)
  (:export
    #:index
    #:make-parameters))
(in-package :fcl-dt.util)


(deftype index ()
  `(integer 0 ,array-total-size-limit))

(defun make-parameters (n)
  (check-type n index)
  (loop :for i :from 0 :below n
        :collect (symbolicate "%" (write-to-string i))))
