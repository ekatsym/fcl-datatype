(defpackage fcl-datatype
  (:nicknames :fcl-dt)
  (:export #:defdata
           #:data=))
